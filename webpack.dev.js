const webpack = require("webpack");
const path = require("path");

module.exports = {
    mode: "development",
    optimization: {
        minimize: false,
    },
    output: {
        path: path.resolve(__dirname, "build/dev"),
        pathinfo: false,
        filename: "[name].js",
        devtoolModuleFilenameTemplate: "../[resource-path]",
    },
    devtool: "eval-source-map",
    devServer: {
        historyApiFallback: true,
        contentBase: "./build",
        open: true,
        compress: true,
        hot: true,
        port: process.env.DS_PORT || 8080
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre",
            },
        ],
    },
    stats: true,
};
