const mergeWith = require("lodash.mergewith");

const mergeCustomizer = (objValue, srcValue) => {
    if (Array.isArray(objValue)) {
        return objValue.concat(srcValue);
    }
};

module.exports = function() {
    const env = process.env.NODE_ENV === "production" ? "prod" : "dev";
    const commonConfig = require("./webpack.common.js");
    const envConfig = require(`./webpack.${ env }.js`);

    return Object.assign({}, mergeWith(commonConfig, envConfig, mergeCustomizer));
};
