const TerserJSPlugin = require("terser-webpack-plugin");
const path = require("path");

module.exports = {
    mode: "production",
    plugins: [new TerserJSPlugin({})],
    optimization: {
        minimize: true,
        removeAvailableModules: true,
        removeEmptyChunks: true,
        splitChunks: {
            cacheGroups: {
                vendor: {
                    chunks: "all",
                    name: "vendor",
                    test: /[\\/]node_modules[\\/]/
                }
            }
        },
        minimizer: [
            new TerserJSPlugin({
                terserOptions: {
                    cache: true,
                    ecma: 6,
                    mangle: true,
                    keep_classnames: false,
                    keep_fnames: false,
                    output: {
                        comments: false
                    },
                    parallel: true,
                    safari10: true,
                    toplevel: true,
                    wrap_iife: true
                }
            })
        ]
    },
    output: {
        path: path.resolve(__dirname, "build/prod"),
        filename: "[name].min.js",
        publicPath: "./",
    },
    stats: false,
};
