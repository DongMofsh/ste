import Phaser from "phaser";

import { WorldScene } from "./worldScene";
import { Player } from "./player";
import type { Direction } from "./const";
import { directionIdle, directionLeft, directionRight } from "./const";
import { createStatPanel, setStatPanelText, updateStatPanelText } from "./statPanel";

const gameboyWidth = 2600;
const gameboyHeight = 1522;
const canvasWidth = 1024;
const canvasHeight = 768;
const koof = gameboyHeight / gameboyWidth;
const canvasKoofWidth = gameboyWidth / canvasWidth;
const canvasKoofHeight = canvasHeight / canvasWidth;
const img = new Image();

let resizeTimer: number | undefined;
let worldScene: WorldScene;
let player: Player;

img.src = "gameboy.png";
img.style.opacity = "0";

img.onload = function () {
    const gameboyImage = document.getElementsByClassName("gameboy")[0] as HTMLImageElement;
    const gameboyBox = document.getElementsByClassName("gameBox")[0] as HTMLImageElement;

    if (gameboyImage && gameboyBox) {
        gameboyImage.style.backgroundImage = `url("gameboy.png")`;
        gameboyBox.style.opacity = "1";
    }
};

document.addEventListener("DOMContentLoaded", () => {
    const canvas = document.body.getElementsByTagName("canvas")[0];
    const body = document.body.getBoundingClientRect();
    const gameboy = document.body.getElementsByClassName("gameboy")[0] as HTMLDivElement;

    gameboy.style.width = body.width + "px";
    gameboy.style.height = body.width * koof + "px";

    if (!canvas) {
        throw new Error("Canvas does not found");
    }

    new Phaser.Game({
        type: Phaser.WEBGL,
        width: canvasWidth,
        height: canvasHeight,
        zoom: body.width / gameboyWidth,
        physics: {
            default: "arcade",
            arcade: {
                gravity: { y: 500 },
                debug: false,
            },
        },
        autoFocus: true,
        canvas,
        scene: {
            preload: preload,
            create: create,
            update: update,
        },
        render: {
            mipmapFilter: "LINEAR_MIPMAP_LINEAR",
            clearBeforeRender: false,
            failIfMajorPerformanceCaveat: false,
            roundPixels: false,
            pixelArt: false,
            antialias: true,
            antialiasGL: true,
            transparent: true,
            batchSize: 4096,
            premultipliedAlpha: true,
        },
    });

    window.onresize = function () {
        if (resizeTimer) {
            window.clearTimeout(resizeTimer);
        }

        resizeTimer = window.setTimeout(function () {
            resizeTimer = undefined;
            const body2 = document.body.getBoundingClientRect();

            gameboy.style.width = body2.width + "px";
            gameboy.style.height = body2.width * koof + "px";
            canvas.style.width = body2.width / canvasKoofWidth + "px";
            canvas.style.height = (body2.width / canvasKoofWidth) * canvasKoofHeight + "px";
        }, 1000);
    };
});

function preload(this: Phaser.Scene) {
    worldScene = new WorldScene(this);
    worldScene.preload();

    player = new Player(this);
    player.preload();
}

function create(this: Phaser.Scene) {
    document.body.getElementsByClassName("loading")[0].remove();
    const cam = this.cameras.main;

    createStatPanel(this);

    setStatPanelText({
        score: 0,
        energy: 47,
    });

    worldScene.create();
    player.create(worldScene);
    player.addColliders([worldScene.getObject()]);
    player.collectFruit();

    cam.setBounds(0, 0, 1024 * 3, 768);
    cam.centerOn(2048, 384);
    cam.setZoom(2);

    window.requestAnimationFrame(function gameStart() {
        cam.zoomTo(1, 1000);
        cam.pan(512, 384, 1000);
        cam.startFollow(player.getFollowObject(), false, 0.03, 0);
    });

    let isJumpInProgress = false;

    this.time.addEvent({
        delay: 1000,
        loop: true,
        callback: function () {
            let decEnergy = 1;

            if (!isJumpInProgress && player.isJumped()) {
                decEnergy = -7;
                isJumpInProgress = true;
            } else if (player.getDirection() !== directionIdle) {
                decEnergy = -2;
                isJumpInProgress = false;
            } else if (player.getDirection() === directionIdle) {
                isJumpInProgress = false;
            }

            updateStatPanelText({
                energy: decEnergy,
            });
        },
    });
}

function update(this: Phaser.Scene) {
    player.update();
    worldScene.update();
}
