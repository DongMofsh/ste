const ceil = Math.ceil;
const floor = Math.floor;
const rnd = Math.random;
const round = Math.round;

export function getRandomInt(minV: number, maxV: number) {
    const min = ceil(minV);
    const max = floor(maxV);

    return floor(rnd() * (max - min + 1)) + min;
}

export function getRandomBool() {
    return !round(rnd());
}
