export type Direction = -1 | 0 | 1;
export type FruitType = "apple" | "lemon" | "peach";

export const activeSceneDepth = 1;

export const directionLeft = -1 as const;
export const directionIdle = 0 as const;
export const directionRight = 1 as const;

export const fruitApple = "apple" as const;
export const fruitLemon = "lemon" as const;
export const fruitPeach = "peach" as const;

export type PlayerMovementEvent = {
    velocityX: number;
    velocityY: number;
    direction: Direction;
    isJump?: boolean;
};

export const playerMovementEvent = "player-movement";
export const playerJumpEvent = "player-jump";
