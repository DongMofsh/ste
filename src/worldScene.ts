import Phaser from "phaser";

import { getRandomBool, getRandomInt } from "./utils";
import type { PlayerMovementEvent } from "./const";
import { activeSceneDepth, fruitApple, fruitLemon, fruitPeach, playerJumpEvent, playerMovementEvent } from "./const";

const textureSceneBackground = "scene-assets";

const groundY = 717;

export class WorldScene {
    private readonly scene: Phaser.Scene;

    private sky!: Phaser.GameObjects.TileSprite;
    private mountain!: Phaser.GameObjects.TileSprite;
    private farTrees!: Phaser.GameObjects.TileSprite;

    private world!: Phaser.GameObjects.Group;
    private ground!: Phaser.Physics.Arcade.StaticGroup;
    private background!: Phaser.GameObjects.Group;
    private foreground!: Phaser.GameObjects.Group;

    private velocity = 0.3;
    private farTreesVelocity = 0;
    private farTreesDirection = 0;
    private direction = 1;
    private fruits: Phaser.Physics.Arcade.StaticGroup | undefined;
    private fruitCount = 0;

    constructor(scene: Phaser.Scene) {
        this.scene = scene;
    }

    preload() {
        this.scene.load.setPath("");
        this.scene.load.multiatlas(textureSceneBackground, `${textureSceneBackground}.json`);
        this.scene.load.audio("birds", "birds-chirping.mp3");
    }

    create() {
        this.scene.sound.add("birds");
        this.createWorld();
        this.createInitialScreen();
        this.createFruits(100, 3);
        this.scene.sound.play("birds", {
            volume: 0.8,
            loop: true,
            delay: 0,
        });

        this.scene.events.addListener(playerMovementEvent, (data: PlayerMovementEvent) => {
            this.farTreesVelocity = 0.1;
            this.farTreesDirection = data.direction;
        });

        this.scene.events.addListener(playerJumpEvent, () => {
            if (!this.farTrees || !this.mountain) {
                return;
            }

            const toFY = this.farTrees.tilePositionY - 8;
            const toMY = this.mountain.tilePositionY + 3;
            const duration = 590;

            this.scene.tweens.add({
                targets: [this.farTrees],
                tilePositionY: {
                    from: 0,
                    to: toFY,
                },
                duration: duration,
            });

            this.scene.tweens.add({
                targets: [this.mountain],
                tilePositionY: {
                    from: 0,
                    to: toMY,
                },
                duration: duration,
            });

            setTimeout(() => {
                this.scene.tweens.add({
                    targets: [this.farTrees],
                    tilePositionY: {
                        from: toFY,
                        to: 0,
                    },
                    duration: duration,
                });

                this.scene.tweens.add({
                    targets: [this.mountain],
                    tilePositionY: {
                        from: toMY,
                        to: 0,
                    },
                    duration: duration,
                });
            }, duration);
        });
    }

    createWorld() {
        this.world = this.scene.add.group();
        this.world.setDepth(-10, 0);

        this.sky = this.scene.add.tileSprite(900, 355, 1800, 690, textureSceneBackground, "sky").setScrollFactor(0);
        this.mountain = this.scene.add
            .tileSprite(512, 542, 1024, 286, textureSceneBackground, "mountain")
            .setScrollFactor(0);
        this.farTrees = this.scene.add
            .tileSprite(810, 620, 1800, 250, textureSceneBackground, "far-trees")
            .setScrollFactor(0);

        this.world.add(this.sky);
        this.world.add(this.mountain);
        this.world.add(this.farTrees);

        this.ground = this.scene.physics.add.staticGroup();
        this.background = this.scene.add.group();
        this.foreground = this.scene.add.group();
    }

    createInitialScreen() {
        this.addScreenByType(0, 0).addScreenByType(1).addScreenByType(2);
    }

    addScreenByType(screen = 0, screenType?: 0 | 1 | 2) {
        const sType = screenType !== undefined ? screenType : getRandomInt(0, 2);
        const sbType = screenType !== undefined ? screenType : getRandomInt(0, 2);
        const screenX = screen * 1024;
        const backgroundObjects = [];
        const foregroundObjects = [];

        this.ground
            .create(screenX + 512, groundY, textureSceneBackground, "ground-front")
            .setMass(100000)
            .setSize(1024, 59)
            .setOffset(0, 32);

        let skip = false;

        const randomTrees = Array(getRandomInt(2, 15))
            .fill(0)
            .map((_v, i) => {
                if (skip && getRandomBool()) {
                    skip = false;
                    return;
                }

                const z = getRandomBool();

                skip = true;

                return this.scene.add
                    .sprite(screenX + 80 + 130 * i * 2, 610 + (z ? -10 : 0), textureSceneBackground, "ground-tree2")
                    .setScale(0.3 + (z ? 0.1 : 0))
                    .setFlipX(z);
            })
            .filter(function (v) {
                return v !== undefined;
            }) as Phaser.GameObjects.Sprite[];

        if (sbType === 0) {
            backgroundObjects.push(
                ...[
                    ...randomTrees,
                    this.scene.add
                        .sprite(screenX + 512, 665, textureSceneBackground, "ground-back")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 490, 670, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 610, 610, textureSceneBackground, "ground-tree2")
                        .setScale(0.3)
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 410, 610, textureSceneBackground, "ground-tree2")
                        .setScale(0.3)
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 800, 488, textureSceneBackground, "ground-tree2")
                        .setFlipX(getRandomBool()),
                    this.scene.add.sprite(screenX + 810, 670, textureSceneBackground, "ground-grass"),
                ],
            );
        } else if (sbType === 1) {
            backgroundObjects.push(
                ...[
                    ...randomTrees,
                    this.scene.add
                        .sprite(screenX + 512, 665, textureSceneBackground, "ground-back")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 490, 670, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 710, 610, textureSceneBackground, "ground-tree2")
                        .setScale(0.3)
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(
                            screenX + 900 - (getRandomBool() ? 300 : 0),
                            488,
                            textureSceneBackground,
                            "ground-tree2",
                        )
                        .setFlipX(getRandomBool()),
                    this.scene.add.sprite(screenX + 810, 670, textureSceneBackground, "ground-grass"),
                ],
            );
        } else {
            backgroundObjects.push(
                ...[
                    ...randomTrees,
                    this.scene.add
                        .sprite(screenX + 512, 665, textureSceneBackground, "ground-back")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 290, 670, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 340, 670, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 610, 610, textureSceneBackground, "ground-tree2")
                        .setScale(0.3)
                        .setFlipX(true),
                    this.scene.add
                        .sprite(screenX + 280, 494, textureSceneBackground, "ground-tree2")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 350, 488, textureSceneBackground, "ground-tree2")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 370, 488, textureSceneBackground, "ground-tree2")
                        .setFlipX(getRandomBool()),
                    this.scene.add.sprite(screenX + 910, 670, textureSceneBackground, "ground-grass"),
                ],
            );
        }

        if (sType === 0) {
            foregroundObjects.push(
                ...[
                    this.scene.add
                        .sprite(screenX + 280, 485, textureSceneBackground, "ground-tree1")
                        .setScale(0.85)
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 588, 682, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 210, 687, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 240, 687, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add.sprite(screenX + 310, 687, textureSceneBackground, "ground-grass"),
                ],
            );
        } else if (sType === 1) {
            foregroundObjects.push(
                ...[
                    this.scene.add
                        .sprite(screenX + 80, 485, textureSceneBackground, "ground-tree1")
                        .setScale(0.85)
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 588, 682, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 910, 687, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                ],
            );
        } else {
            foregroundObjects.push(
                ...[
                    this.scene.add
                        .sprite(
                            screenX + 280 + (getRandomBool() ? getRandomInt(0, 400) : 0),
                            485,
                            textureSceneBackground,
                            "ground-tree1",
                        )
                        .setScale(0.85),
                    this.scene.add
                        .sprite(
                            screenX + 340 - (getRandomBool() ? getRandomInt(0, 200) : 0),
                            530,
                            textureSceneBackground,
                            "ground-tree1",
                        )
                        .setScale(0.65)
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 588, 682, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add
                        .sprite(screenX + 910, 687, textureSceneBackground, "ground-grass")
                        .setFlipX(getRandomBool()),
                    this.scene.add.sprite(screenX + 930, 688, textureSceneBackground, "ground-grass"),
                ],
            );
        }

        this.background.addMultiple(backgroundObjects).setDepth(0, 0);
        this.foreground.addMultiple(foregroundObjects).setDepth(5, 0);

        return this;
    }

    createFruits(x: number, screenCount = 1) {
        this.fruitCount = getRandomInt(7, 12 * screenCount);
        this.fruits = this.scene.physics.add.staticGroup();

        for (let i = 0; i < this.fruitCount; i++) {
            const x1 = x + getRandomInt(60, 80);
            const y = 525 + getRandomInt(-50, 90);

            const name = this.getRndFruitName();

            this.fruits
                .create(x1, y, textureSceneBackground, name)
                .setFlipX(!!getRandomInt(0, 1))
                .setSize(27, 40)
                .setOffset(4, 4)
                .setCircle(true)
                .setScale(0.8)
                .setRotation(Phaser.Math.Angle.Normalize(Math.random() * (!!getRandomInt(0, 1) ? -1 : 1)))
                .setName(name)
                .setImmovable();

            x = x1;
        }

        this.fruits.setDepth(activeSceneDepth, 0);
        this.scene.tweens.add({
            targets: this.fruits.getChildren(),
            scaleX: {
                from: 0.8,
                to: 1,
            },
            scaleY: {
                from: 0.8,
                to: 1,
            },
            duration: 800,
            ease: Phaser.Math.Easing.Sine.InOut,
            repeat: -1,
            yoyo: true,
            delay: function () {
                return getRandomInt(100, 1000);
            },
        });
    }

    getFruits() {
        return this.fruits as Phaser.Physics.Arcade.StaticGroup;
    }

    getRndFruitName() {
        const fruits = [fruitApple, fruitLemon, fruitPeach];

        return fruits[getRandomInt(0, 2)];
    }

    getObject() {
        if (!this.ground) {
            throw new Error("Ground does not found");
        }

        return this.ground;
    }

    setFarTrees(velocity: number, direction?: number) {
        this.farTreesVelocity = velocity;
        this.farTreesDirection = direction || 1;
    }

    update() {
        if (!this.sky || !this.farTrees) {
            return;
        }

        this.sky.tilePositionX += this.velocity * this.direction;

        if (this.farTreesDirection) {
            this.farTrees.tilePositionX += this.farTreesVelocity * this.farTreesDirection;
        }
    }
}
