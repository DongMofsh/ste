import Phaser from "phaser";

import type { Direction } from "./const";
import {
    activeSceneDepth,
    directionIdle,
    directionLeft,
    directionRight,
    fruitApple,
    fruitLemon,
    fruitPeach,
    FruitType,
    playerJumpEvent,
    playerMovementEvent,
} from "./const";
import { WorldScene } from "./worldScene";
import { updateStatPanelText } from "./statPanel";
import { getRandomBool } from "./utils";

type SceneObject = Phaser.GameObjects.GameObject | Phaser.GameObjects.Group | Phaser.Physics.Arcade.StaticGroup;

const texturePlayer = "player";

const pickupFruitAudio = "pickupFruit";
const footstepAudio = "footstep";
const jumpAudio = "jump";
const animIdle = "idle";
const animRun = "run";
const soundPlay = "play";
const soundStop = "stop";
const footGroundY = 564.5;

const energies: Record<FruitType, number> = {
    [fruitApple]: 1,
    [fruitLemon]: 2,
    [fruitPeach]: 5,
};

let lastScreenIndex = 3;

export class Player {
    private readonly scene!: Phaser.Scene;
    private readonly p!: Phaser.Physics.Arcade.ArcadePhysics;
    private worldScene!: WorldScene;
    // @ts-expect-error: TS2564: Property 'player' has no initializer and is not definitely assigned in the constructor.
    private player: Phaser.Physics.Arcade.Sprite;
    private velocityX = 180;
    private velocityY = -350;
    private cursors: Phaser.Types.Input.Keyboard.CursorKeys;
    private direction: Direction = directionIdle;
    private step: Phaser.Sound.BaseSound | undefined;
    private isStepPlaying = false;
    private actualDirection: Direction = directionIdle;

    constructor(scene: Phaser.Scene) {
        this.scene = scene;
        this.p = this.scene.physics;
        this.cursors = this.scene.input.keyboard.createCursorKeys();
    }

    preload() {
        this.scene.load.setPath("");
        this.scene.load.multiatlas(texturePlayer, `${texturePlayer}.json`);
        this.scene.load.audio(pickupFruitAudio, "pickup-fruit.mp3");
        this.scene.load.audio(footstepAudio, "footstep.mp3");
        this.scene.load.audio(jumpAudio, "jump.mp3");
    }

    getFollowObject() {
        return this.player;
    }

    create(worldScene: WorldScene) {
        this.worldScene = worldScene;
        this.player = this.p.add.sprite(50, 555, texturePlayer, "01");
        this.player.setMass(50).setSize(58, 135).setDepth(activeSceneDepth);

        const idleFrameNames = this.scene.anims.generateFrameNames(texturePlayer, {
            frames: [1, 2, 3, 4, 5, 6, 5, 4, 3, 2],
            zeroPad: 2,
        });

        this.scene.anims.create({
            key: animIdle,
            frames: idleFrameNames,
            frameRate: 7,
            repeat: -1,
        });

        const runFrameNames = this.scene.anims.generateFrameNames(texturePlayer, {
            frames: [11, 12, 13, 14, 15, 16, 17],
            zeroPad: 2,
        });

        this.scene.anims.create({
            key: animRun,
            frames: runFrameNames,
            frameRate: 14,
            repeat: -1,
        });

        this.step = this.scene.sound.add(footstepAudio, {
            volume: 0.7,
            loop: true,
        });
    }

    getY() {
        return this.player.body.y;
    }

    getX() {
        return this.player.body.x;
    }

    getDirection() {
        return this.actualDirection;
    }

    addColliders(gameObjects: SceneObject[]) {
        gameObjects.forEach((gameObject) => {
            this.scene.physics.add.collider(this.player, gameObject);
        });
    }

    soundStep(a: string) {
        if (!this.step) {
            return;
        }

        if (a === soundPlay) {
            if (this.getY() === footGroundY && !this.isStepPlaying) {
                this.step.play();
                this.isStepPlaying = true;
            } else if (this.getY() !== footGroundY && this.isStepPlaying) {
                this.step.stop();
                this.isStepPlaying = false;
            }
        } else if (a === soundStop) {
            this.step.stop();
            this.isStepPlaying = false;
        }
    }

    moveLeft() {
        this.scene.cameras.main.setFollowOffset(300);
        this.direction = directionLeft;
        this.player.setVelocityX(this.velocityX * this.direction).setFlipX(true);
        this.soundStep(soundPlay);
        this.setActualDirection(this.direction);

        if (this.player.anims.getName() !== animRun) {
            this.player.anims.play(animRun);
            this.scene.events.emit(playerMovementEvent, {
                direction: this.actualDirection,
                velocityX: this.player.body.velocity.x,
                velocityY: this.player.body.velocity.y,
            });
        }
    }

    moveRight() {
        this.scene.cameras.main.setFollowOffset(-300);
        this.direction = directionRight;
        this.player.setVelocityX(this.velocityX * this.direction).setFlipX(false);
        this.soundStep(soundPlay);
        this.setActualDirection(this.direction);

        if (this.player.anims.getName() !== animRun) {
            this.player.anims.play(animRun);
            this.scene.events.emit(playerMovementEvent, {
                direction: this.actualDirection,
                velocityX: this.player.body.velocity.x,
                velocityY: this.player.body.velocity.y,
            });
        }
    }

    idle() {
        this.player.setVelocityX(0).setFlipX(this.direction === directionLeft);
        this.setActualDirection(directionIdle);

        if (this.player.anims.getName() !== animIdle) {
            this.player.anims.play(animIdle);
            this.soundStep(soundStop);
            this.scene.events.emit(playerMovementEvent, {
                direction: this.actualDirection,
                velocityX: this.player.body.velocity.x,
                velocityY: this.player.body.velocity.y,
            });
        }
    }

    jump() {
        if (this.player.body.blocked.down) {
            this.player.setVelocityY(this.velocityY);
            this.soundStep(soundStop);
            this.scene.sound.play(jumpAudio, {
                volume: 0.8,
                loop: false,
            });
            this.scene.events.emit(playerJumpEvent, {
                direction: this.actualDirection,
                velocityX: this.player.body.velocity.x,
                velocityY: this.player.body.velocity.y,
                isJump: true,
            });

            this.scene.cameras.main.zoomTo(1.2, 700, "Linear", false, () => {
                window.setTimeout(() => {
                    this.scene.cameras.main.zoomTo(1, 600);
                }, 701);
            });
        }
    }

    die(gameObject: SceneObject) {
        this.scene.physics.add.collider(
            this.player,
            gameObject,
            () => {
                this.scene.physics.pause();
                this.player.setTint(0xff0000);
                this.player.anims.play(animIdle);
            },
            () => {
                /* noop */
            },
            this,
        );
    }

    collectFruit() {
        if (!this.player) {
            throw new Error("Does not found player");
        }

        const gameObject: SceneObject = this.worldScene.getFruits();
        const callback: (player: SceneObject, item: SceneObject) => void = this.collectFruitHandler.bind(this);

        this.scene.physics.add.overlap(
            gameObject,
            this.player,
            (_player, item) => {
                item.setActive(false);
                item.destroy();
                this.scene.sound.play(pickupFruitAudio, {
                    volume: 0.5,
                    loop: false,
                });

                if (callback) {
                    callback(_player, item);
                }
            },
            () => {
                /* noop */
            },
            this,
        );
    }

    update() {
        if (this.cursors.left && this.cursors.left.isDown) {
            if (this.getX() < 5) {
                this.idle();
                return;
            }

            this.moveLeft();
        } else if (this.cursors.right && this.cursors.right.isDown) {
            if (this.getX() > 1024 * (lastScreenIndex - 1)) {
                this.worldScene.addScreenByType(lastScreenIndex);
                this.worldScene.addScreenByType(lastScreenIndex + 1);
                this.worldScene.addScreenByType(lastScreenIndex + 2);
                this.worldScene.createFruits(1024 * lastScreenIndex - 512, 2 + (getRandomBool() ? 1 : 0));
                lastScreenIndex += 2;
                this.scene.cameras.main.setBounds(0, 0, 1024 * lastScreenIndex, 768);
                this.collectFruit();

                return;
            }

            this.moveRight();
        } else {
            this.idle();
        }

        if (!this.isJumped() && this.cursors.up && this.cursors.up.isDown) {
            this.jump();
        }
    }

    isJumped() {
        return this.getY() !== footGroundY;
    }

    private collectFruitHandler(_p: SceneObject, f: SceneObject) {
        const energy = energies[f.name as FruitType];

        updateStatPanelText({
            score: energy * 3,
            energy: energy,
        });
    }

    private setActualDirection(d: Direction) {
        this.actualDirection = d;
    }
}
