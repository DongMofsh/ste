import Phaser from "phaser";

type StatPanel = { score?: number; energy?: number };

let originScore = -1;
let scoreText: Phaser.GameObjects.Text;
let originEnergy = -1;
let energyText: Phaser.GameObjects.Text;

export function createStatPanel(scene: Phaser.Scene) {
    scoreText = scene.add.text(16, 16, "", {
        fontFamily: "Tahoma",
        fontSize: "32px",
        color: "#161616",
    });
    scoreText.setDepth(10).setScrollFactor(0);
    energyText = scene.add.text(460, 16, "", {
        fontFamily: "Tahoma",
        fontSize: "32px",
        color: "#161616",
    });
    energyText.setDepth(10).setScrollFactor(0);
}

export function setStatPanelText({ score, energy }: StatPanel) {
    if (score !== undefined && originScore !== score) {
        originScore = score || 0;
        scoreText.setText(`Score: ${originScore}`);
    }

    if (energy !== undefined && originEnergy !== energy) {
        originEnergy = energy || 0;
        energyText.setText(`Energy: ${originEnergy}`);
    }
}

export function updateStatPanelText({ score, energy }: StatPanel) {
    const update: StatPanel = {};

    if (score) {
        update.score = originScore + score;
    }

    if (energy) {
        update.energy = originEnergy + energy;
    }

    setStatPanelText(update);
}
